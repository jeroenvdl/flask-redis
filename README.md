Python application with Redis
=============================

Installation
============
Clone this repository to your local PC.
```
git clone 
```

Create a virtual environment in Python
```
python3 -m venv environment
```

Install the requirements
```
pip install -r flask/requirements.txt
```