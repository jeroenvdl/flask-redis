"""Application configuration"""
from os import environ, path
basedir = path.abspath(path.dirname(__file__))

class Config(object):
    """Set Flask configuration variables from environment."""
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_APP = environ.get('FLASK_APP')
    FLASK_ENV = environ.get("FLASK_ENV")
    FLASK_RUN_HOST = environ.get("FLASK_RUN_HOST")
    FLASK_DEBUG = environ.get("FLASK_DEBUG", default="false")

    # Database configuration
    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI') or 'sqlite:///' + path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get('SQLALCHEMY_TRACK_MODIFICATIONS', default="false")

    POSTS_PER_PAGE = 10